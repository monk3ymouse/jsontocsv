import json

listtoWriteCSV =[]
twistlockrawresult="cs-results.json"
reportCSV="report.csv"

def KSseverity(cvss):
	if cvss >= 9.0:
		ksSeverity="Critical"
	elif 7.0 <= cvss >= 8.9:
		ksSeverity="High"
	elif 4.0 <= cvss >= 6.9:
		ksSeverity="Medium"
	elif 0.1 <= cvss >= 3.9:
		ksSeverity="Low"
	else:
		ksSeverity="Low"
	return ksSeverity

def twistlockJSONparser(datastore):
	for i in range(len(datastore[0]['entityInfo']['vulnerabilities'])):
		cve = datastore[0]['entityInfo']['vulnerabilities'][i]['cve']
		cvss = datastore[0]['entityInfo']['vulnerabilities'][i]['cvss']
		twistlockSeverity = datastore[0]['entityInfo']['vulnerabilities'][i]['severity']
		ksSeverity = KSseverity(cvss)
		packageName = datastore[0]['entityInfo']['vulnerabilities'][i]['packageName']
		packageVersion = datastore[0]['entityInfo']['vulnerabilities'][i]['packageVersion']
		description = datastore[0]['entityInfo']['vulnerabilities'][i]['description']
		oneline = cve+","+str(cvss)+","+twistlockSeverity+","+ksSeverity+","+packageName+","+packageVersion+","+description
		listtoWriteCSV.append(oneline)

def writeCSV():
	header=str('CVE,cvss,twistlockSeverity,ksSeverity,packageName,description')
	with open(reportCSV, 'w') as f:
		#writeheader
		f.write(header)
		#writeline
		for line in listtoWriteCSV:
			f.write('\n')
			f.write(line)

def main():
	f = open(twistlockrawresult,'r')
	message = f.read()
	datastore = json.loads(message)
	twistlockJSONparser(datastore) 
	writeCSV()
	f.close() 
	#print(listtoWriteCSV)

if __name__ == "__main__":
    main()